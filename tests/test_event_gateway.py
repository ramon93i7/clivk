from vkapi import VK
from vk_events_gateway import VKEventsGateway
from event_handling import EventHandlingGateway


class PrintGW(EventHandlingGateway):
    def __init__(self):
        self.__cnt = 0
        super(PrintGW, self).__init__()

    def handle(self, update):
        print '[%d] %s' % (self.__cnt, str(update))
        self.__cnt += 1

AT = 'd753291a82d7a226f4fa88bd679c3855883469ee40496573264a5a2ab5fcd67c66e7ddfe07594a37e83e2'
vkapi = VK(AT)
with VKEventsGateway(**vkapi.get_LongPoll_credentials()) as vkgw, PrintGW() as pgw:
    # Body
    vkgw.link_to_event_gateway(pgw)
    map(lambda x: x.start(), [vkgw, pgw])

    import time
    print 'Started'
    while True:
        time.sleep(5)
