#!/usr/bin/python
import requests
from lxml.html import fromstring as parse_html
import re
from getpass import getpass

class VKAuth(object):
    auth_rx = re.compile(
        r'https://oauth.vk.com/blank.html#access_token=(?P<access_token>[a-f0-9]{1,})&expires_in=86400&user_id=(?P<user_id>[0-9]{1,})'
    )
    auth_url = 'https://oauth.vk.com/authorize'
    auth_params = {
        'client_id': '5058131',
        'display': 'wap',
        'redirect_uri': 'https://oauth.vk.com/blank.html',
        'response_type': 'token',
        'scope': 'friends,messages,status',
        'v': '5.37'
    }
    session = None

    login_url = 'https://login.vk.com/?act=login&soft=1&utf8=1'
    mvk_url = 'https://m.vk.com'

    @staticmethod
    def get_credentials(email, pswd):
        # New session
        VKAuth.session = requests.Session()

        # Initial OAuth request
        oauth_resp = VKAuth.session.request('GET', VKAuth.auth_url, VKAuth.auth_params)
        oauth_resp_page = parse_html(oauth_resp.content)

        # Prepare login parameters
        login_form = oauth_resp_page.forms[0]
        login_params = dict(login_form.fields)
        login_params.update({
            'email': email,
            'pass': pswd
        })
        # Login request
        login_resp = VKAuth.session.request('POST', VKAuth.login_url, login_params)

        # Match credentials
        credentials_match = VKAuth.auth_rx.match(str(login_resp.url))

        # Error if response is not with code request page and target-AT page
        if VKAuth.mvk_url not in login_resp.url and credentials_match == None:
            return None

        # If special security code required
        while credentials_match == None:
            # Gain special security code
            login_resp_page = parse_html(login_resp.content)
            code_form = login_resp_page.forms[0]
            code_params = dict(code_form.fields)
            code_params['code'] = raw_input('code: ')

            code_resp = VKAuth.session.request('POST', VKAuth.mvk_url + code_form.action, code_params)
            credentials_match = VKAuth.auth_rx.match(str(code_resp.url))

        return credentials_match.groupdict()

def auth():
    def pars():
        return {
            'email': raw_input('email: '),
            'pswd': getpass('pass: ')
        }

    res = None
    while not res:
        res = VKAuth.get_credentials(**pars())
        if not res:
            print 'Bad email/password'
    return res

if __name__ == '__main__':
    print auth()
