class EventCodes:
    # Messages
    CHANGE_MSG_FLAG = 'change_message_flag'
    SET_MSG_FLAG = 'set_message_flag'
    RESET_MSG_FLAG = 'reset_message_flag'
    READ_INPUT_MSG = 'read_input_messages'
    READ_OUTPUT_MSG = 'read_output_messages'
    READ_MSGS = 'read_messages'
    ADD_NEW_MSG = 'add_new_message'
    # Friends
    FRD_ON = 'friend_online'
    FRD_OFF = 'friend_offline'
    FRD_STATUS = 'friend_status'
    FRD_TYPING = 'friend_typing'
    # From View
    SEND_MSG = 'send_message'
    MARK_AS_READ = 'mark_as_read'
    REQUEST_CHAT = 'request_chat'
