import requests

class VK:
    __vk_method_url = 'https://api.vk.com/method/%s'
    # Init
    def __init__(self, access_token):
        self.__access_token = access_token

    @staticmethod
    def __do_safe(http_method_request, url, payload):
        request_attempts = 5
        for _ in xrange(request_attempts):
            try:
                return http_method_request(url, params=payload)
            except requests.exceptions.Timeout:
                logging.warning('Timeout error.')

    @staticmethod
    def __do_safe_get(url, payload):
        return VK.__do_safe(requests.get, url, payload)

    @staticmethod
    def __do_safe_post(url, payload):
        return VK.__do_safe(requests.post, url, payload)


    @staticmethod
    def request_json_api(url, params):
        try:
            response = VK.__do_safe_post(url, params).json()
            if 'error' in response:
                raise NameError, 'VK API error: %s' % str(response['error'])
        except ValueError:
            raise NameError, 'JSON decoding error.'
        else:
            return response

    @staticmethod
    def request_api_method(method, params):
        url = VK.__vk_method_url % method
        params['v'] = '5.3'
        return VK.request_json_api(url, params)

    # Public
    def get_usr(self):
        data = VK.request_api_method('users.get', {
            'access_token': self.__access_token
        })
        return data # {id, first_name, last_name}

    def get_friends(self):
        return VK.request_api_method('friends.get', {
            'order': 'hints',
            'fields': 'online',
            'access_token': self.__access_token
        })['response']['items'] # [{id, first_name, last_name, online}, ... ]

    def search_friend(self, query):
        return VK.request_api_method('friends.search', {
            'q': query,
            'access_token': self.__access_token
        })['response']['items'] # [{id, first_name, last_name}, ... ]

    def send_msg(self, user_id, message):
        data = VK.request_api_method('messages.send', {
            'user_id': str(user_id),
            'message': message,
            'access_token': self.__access_token
        })
        return data # message_id

    def read_msg(self, msg_id_list):
        message_ids = ','.join(map(str, msg_id_list))
        return VK.request_api_method('messages.markAsRead', {
            'message_ids': str(message_ids),
            'access_token': self.__access_token
        })

    def get_history(self, user_id, count = 10, start_msg_id = 0):
        data = VK.request_api_method('messages.getHistory', {
            'count': count,
            'user_id': user_id,
            'start_message_id': start_msg_id,
            'access_token': self.__access_token
        })
        return data['response']['items'] # [{id, body, user_id, from_id, data, read_state, out}, ... ]

    def set_online(self):
        return VK.request_api_method('account.setOnline', {
            'access_token': self.__access_token
        })

    def get_LongPoll_credentials(self):
        data = VK.request_api_method('messages.getLongPollServer', {
            'use_ssl': 1,
            'access_token': self.__access_token
        })
        return data['response']

class BasePoller(object):
    # Init
    def __init__(self, server, key, ts):
        self.__server = 'https://' + server
        self.__params = {
            'act': 'a_check',
            'key': key,
            'wait': '15',
            'mode': '2',
            'ts': ts
        }

    def check_updates(self):
        data =  VK.request_json_api(self.__server, self.__params)
        # Update timestamp
        self.__params['ts'] = data['ts']
        # Return updates
        return data['updates']

    def handle(self, update):
        raise NotImplementedError, 'Handling of poll-updates'

    def stop_condition(self):
        raise NotImplementedError, 'Stop polling condition'

    def serve(self):
        while not self.stop_condition():
            map(self.handle, self.check_updates())

