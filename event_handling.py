from Queue import Queue
from managable_thread import ManagableThread

class EventQueue(Queue):
    def __init__(*args, **kwargs):
        self = args[0]
        self.__handling_thread = None
        Queue.__init__(*args, **kwargs)

    def link_handler(self, handler):
        self.__handling_thread = handler
        handler.next_event = self.get
        handler.no_events = self.empty

    def put(*args, **kwargs):
        self = args[0]
        was_empty = self.empty()
        res = Queue.put(*args, **kwargs)
        # Queue was empty. It has a thread to monitor and that thread is paused.
        if was_empty and self.__handling_thread and self.__handling_thread.is_paused():
            self.__handling_thread.resume()
        return res


class EventHandler(ManagableThread):
    def handle(self, event):
        raise NotImplementedError, 'Event handling'

    def run(self):
        assert(self.next_event and self.no_events)
        while not self.stopped():
            if self.no_events():
                self.pause()
            else:
                self.handle(self.next_event())

class EventHandlingGateway(EventHandler):
    def __init__(self):
        # Configure event queue
        self.__queue = EventQueue()
        self.__queue.link_handler(self)
        self.register_event = self.__queue.put
        # Continue init
        super(EventHandlingGateway, self).__init__()

