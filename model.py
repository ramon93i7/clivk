class ClientModel:
    def __init__(self):
        self.user_id = None
        self.__friends = None # <friend_id>: { name: <friend_name>, status: <is online>, chat: [ <messages> ] }
        self.__messages = None

    @staticmethod
    def __safe_friend_request(fn):
        def nfn(*args, **kwargs):
            self, fid = args[0:2]
            if fid in self.__friends:
                return fn(*args, **kwargs) or True
            else:
                return False

    @staticmethod
    def __safe_chat_request(fn):
        def nfn(*args, **kwargs):
            self, fid = args[0:2]
            if fid in self.__friends and self.__friends[fid].chat:
                return fn(*args, **kwargs) or True
            else:
                return False
        return nfn

    @staticmethod
    def __safe_message_request(fn):
        def nfn(*args, **kwargs):
            self, mid  = args[0:2]
            if mid in self.__messages:
                return fn(*args, **kwargs) or True
            else:
                return False

    @ClientModel.__safe_chat_request
    def get_chat(self, fid):
        return self.__friends[fid].chat

    @ClientModel.__safe_chat_request
    def add_message(self, fid, msg):
        chat = self.__friends[fid].chat
        chat.append(msg)
        self.__messages[msg['message_id']] = msg

    @ClientModel.__safe_message_request
    def get_message(self, mid):
        return self.__messages[mid]

    @ClientModel.__safe_message_request
    def update_message_flags(self, mid, flags):
        msg = self.__messages[mid]
        msg['flags'].update(flags)

    @ClientModel.__safe_message_request
    def mark_message_as_read(self, mid):
        msg = self.__message[mid]
        msg['flags']['unread'] = False

    def mark_msg_range_as_read(io_filter, from_id, to_id):
        is_output = io_filter == 'output'
        updated_msgs = []
        for mid in map(str, xrange(int(from_id), int(to_id) + 1)):
            msg = self.get_message(mid)
            if msg and msg['flags']['outbox'] == is_output:
                msg['flags']['unread'] = False
                updated_msgs.append(mid)
        return updated_msgs

    @ClientModel.__safe_friend_request
    def change_status(self, fid, status):
        self.__friends[fid]['status'] = status

    @ClientModel.__safe_friend_request
    def init_chat(self, fid, msgs):
        pass # TODO
