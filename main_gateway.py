from event_handling import EventHandlingGateway
from constants import EventCodes
from vkapi import VK

class MainGateway(EventHandlingGateway):
    def __init__(self, access_token):
        EventHandlingGateway.__init__(self)
        self.__vkapi = VK(access_token)
        self.__handler_table = {
            EventCodes.CHANGE_MSG_FLAG: self.__change_msg_flags,
            EventCodes.READ_MSGS: self.__read_messages,
            EventCodes.ADD_NEW_MSG: self.__add_message,
            EventCodes.FRD_STATUS: self.__change_friend_status,
            EventCodes.FRD_TYPING: self.__friend_typing,
            EventCodes.SEND_MSG: self.__send_message,
            EventCodes.MARK_AS_READ: self.__mark_as_read,
            EventCodes.REQUEST_CHAT: self.__request_chat
        }

    # Message flags management
    def __change_msg_flags(self, params):
        # params = {'message_id', 'flags'}
        pass

    # Read Messages
    def __read_messages(self, params):
        # params = {'filter', 'from_id', 'to_id'}
        pass

    # Add Message
    def __add_message(self, params):
        # params = {'message_id', 'flags', 'friend_id', 'timestamp', 'text'}
        pass

    # Friend management
    def __change_friend_status(self, params):
        # params = {'friend_id', 'status'}
        pass

    def __friend_typing(self, params):
        # params = {'friend_id', 'status'}
        # Lazy UI update: only for foreground chat
        pass

    # User events management
    def __send_message(self, params):
        self.__vkapi.send_msg(params['friend_id'], params['text'])

    def __mark_as_read(self, params):
        self.__vkapi.read_msg(params['message_ids'])

    def __request_chat(self, params):
        chat = self.__vkapi.get_history(params['friend_id'])
        pass

    def handle(self, event):
        event_code, params = event
        self.__handler_table[event_code](params)
