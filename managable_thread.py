import threading

class ManagableThread(threading.Thread):
    def __init__(self):
        self.__lock = threading.Lock()
        self.__lock.acquire()
        self.__paused = False
        self.__access_mutex = threading.RLock()
        self.__terminated = False
        super(ManagableThread, self).__init__()

    # Access manipulation
    def __safe_access(fn):
        def nfn(*args, **kwargs):
            self = args[0]
            self.__access_mutex.acquire()
            res = fn(*args, **kwargs)
            self.__access_mutex.release()
            return res
        return nfn

    def __capture_access(self):
        self.__access_mutex.acquire()

    def __release_access(self):
        self.__access_mutex.release()

    # Thread management

    ## Thread is paused (predicate)
    @__safe_access
    def is_paused(self): # Outer blocking version
        return self.__paused

    ## Pause thread
    def pause(self):
        self.__capture_access()
        if not self.__paused:
            self.__paused = True
            self.__release_access()
            self.__lock.acquire()
        else:
            self.__release_access()

    ## Resume thread
    @__safe_access
    def resume(self):
        if self.__paused:
            self.__paused = False
            self.__lock.release()

    ## Thread is stopped (predicate)
    @__safe_access
    def stopped(self):
        return self.__terminated

    ## Stop thread
    @__safe_access
    def stop(self):
        self.__terminated = True
        if self.is_paused():
            self.resume()

    # Context manager interface
    def __enter__(self):
        return self

    def __exit__(*args, **kwargs):
        self = args[0]
        self.stop()

