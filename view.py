#!/usr/bin/python

import urwid as UI
import time

def dtime(timestamp):
    return time.strftime('[%H:%M:%S]', time.localtime(timestamp))

def GLineBox(wgt):
    corners = { 'tlcorner': u'\u256d', 'trcorner': u'\u256e', 'brcorner': u'\u256f', 'blcorner': u'\u2570' }
    return UI.LineBox(wgt, **corners)

def GPadding(wgt, width_part = None, align='center'):
    # Settings
    prc = 0.98
    base = 0.75
    coef = base * (1 - prc)
    #
    # Normalize
    width_part = width_part or base
    # Calculate percentage
    if width_part != None:
        prc = (1 - coef / width_part) * 100
    # Wrap widget
    return UI.Padding(wgt, width=('relative', prc), align=align)

def GPaddedBox(wgt, width_part=None, align='center'):
    return GLineBox(GPadding(wgt, width_part, align))

class FriendWgt(UI.Columns):
    def __init__(self, fid, name, status, unread_msgs):
        self.fid = fid
        if unread_msgs:
            name = '%s (+%d)' % (name, unread_msgs)
        super(FriendWgt, self).__init__([
            UI.Text(('friends.status', FriendWgt.__build_status(status))),
            ('weight', 8, UI.Text(('friends.name.not_selected', name), wrap='clip')),
            #UI.Text(('friends.unread_messages', unread_msgs and str(unread_msgs) or ''), wrap='clip')
        ])

    @staticmethod
    def __build_status(status):
        return u'\u25cf' if status else u'\u25cb'

    def change_status(self, new_status):
        self.contents[0].set_text(FriendWgt.__build_status(new_status))

    def change_unread_msgs(self, count):
        #self.contents[2].set_text(count or '')
        name = self.contents[1].get_text().split()
        if count:
            name = '%s %s (+%d)' % (name[0], name[1], count)
        else:
            name = ' '.join(name[0:2])
        self.contents[1].set_text(name)

    def select(self):
        self[1].set_text(('friends.name.selected', self[1].text))

    def unselect(self):
        self[1].set_text(('friends.name.not_selected', self[1].text))

class MessageWgt(UI.Text):
    def __init__(self, msg, from_name, timestamp):
        super(MessageWgt, self).__init__(
            [('chat.time', dtime(timestamp)), ('chat.name', u' %s: ' % from_name), ('chat.msg', msg)],
            wrap='space'
        )

class WidgetListPool(UI.ListBox):
    class Walker(UI.SimpleFocusListWalker):
        def focused_element(self):
            fwgt, focus = self.get_focus()
            return fwgt

        def next_element(self):
            fwgt, focus = self.get_focus()
            focus = min(len(self) - 1, focus + 1)
            return self[focus], focus

        def prev_element(self):
            fwgt, focus = self.get_focus()
            focus = max(0, focus - 1)
            return self[focus], focus

    def __init__(self, init = []):
        body = WidgetListPool.Walker(init)
        super(WidgetListPool, self).__init__(body)


class FriendListFrame(UI.Frame):
    def __init__(self, parent, friends, width_part=0.25):
        self.__parent = parent
        # Form links and FriendWgt's
        friend_wgts = []
        self.__friends = {}
        import random
        for friend in friends:
            fwgt = FriendWgt(
                friend['id'],
                u'{first_name} {last_name}'.format(**friend),
                friend['online'] == 1,
                random.randrange(10)
            )
            friend_wgts.append(fwgt)
            self.__friends[friend['id']] = fwgt

        # Pack into WidgetListPool
        self.__friend_wgts = WidgetListPool(friend_wgts)

        # Focus management bindings
        self.__move_focus_actions = {
            'up': self.__friend_wgts.body.prev_element,
            'down': self.__friend_wgts.body.next_element,
        }

        # Set focus
        self.__move_focus('up') # Hack with implementation

        # Pack into box
        super(FriendListFrame, self).__init__(
            GPaddedBox(self.__friend_wgts, width_part)
        )

    def __move_focus(self, direction):
        # Remove selection from focused widget
        fwgt = self.__friend_wgts.body.focused_element()
        fwgt.unselect()
        # Move focus
        fwgt, focus = self.__move_focus_actions[direction]()
        fwgt.select()
        self.__friend_wgts.set_focus(focus)

    def friend_on_focus(self):
        return self.__friend_wgts.get_focus()[0].fid

    def keypress(self, size, key):
        if key in ('up', 'down', 'j', 'k'):
            self.__move_focus({
                'up': 'up',
                'down': 'down',
                'j': 'down',
                'k': 'up'
            }[key])
        elif key == ' ':
            self.__parent.change_chat(self.friend_on_focus())
        elif key == 'enter':
            self.__parent.change_chat(self.friend_on_focus())
            self.__parent.focus_edit()
        else:
            return super(FriendListFrame, self).keypress(size, key)

class ChatWgt(WidgetListPool):
    def __init__(self, friend):
        self.__friend_name = u'{first_name} {last_name}'.format(**friend)
        super(ChatWgt, self).__init__()

    def add_message(self, msg, timestamp, from_name):
        self.body.append(MessageWgt(msg, from_name, timestamp))
        self.set_focus(len(self.body) - 1)

class ChatFrame(UI.Frame):
    def __init__(self, parent, chat):
        self.__parent = parent
        super(ChatFrame, self).__init__(chat, footer=GPaddedBox(UI.Edit()))

    def set_chat(self, chat):
        self.body = chat

    def keypress(self, size, key):
        focus = self.focus
        chat = self.body.base_widget
        edit = self.footer.base_widget
        if focus == self.footer:
            if key == 'enter':
                # DO SEND MESSAGE
                chat.add_message(edit.get_edit_text(), time.time(), 'tester')
                edit.set_edit_text('')
            else:
                edit.keypress(size[0:1], key)
        else:
            chat.keypress(size, key)

class ClientView(UI.Columns):
    def __init__(self, friends):
        # Data widgets
        self.__chats = { friend['id']: GPaddedBox(ChatWgt(friend)) for friend in friends }
        # Layout
        self.__friend_lst_frame = FriendListFrame(self, friends)
        self.__chat_frame = ChatFrame(self, self.__chats[self.__friend_lst_frame.friend_on_focus()])
        self.__edit = self.__chat_frame.footer

        super(ClientView, self).__init__([
            self.__friend_lst_frame,
            ('weight', 3, self.__chat_frame)
        ])

    ### CHAT MANAGEMENT ###
    def change_chat(self, fid):
        self.__chat_frame.set_chat(self.__chats[fid])

    def add_message(self, msg):
        chat = self.__chats[msg['id']]
        chat.add_message(msg['text'])
        # TODO: update friend list

    def init_chat(self, chat):
        pass

    def mark_as_read(self, mid):
        pass

    def change_friend_status(self, fid, status):
        pass

    ### FOCUS INTERFACE ###
    def focus_friend_lst(self):
        self.set_focus(self.__friend_lst_frame)
        self.__friend_lst_frame.focus_position = 'body'

    def focus_chat(self):
        self.set_focus(self.__chat_frame)
        self.__chat_frame.focus_position = 'body'

    def focus_edit(self):
        self.set_focus(self.__chat_frame)
        self.__chat_frame.focus_position = 'footer'

    def change_focus(self):
        fc = self.focus
        fframe = self.__friend_lst_frame
        chatframe = self.__chat_frame
        # Logic
        if fc == self.__friend_lst_frame:
            self.focus_edit()
        elif fc == self.__chat_frame:
            fc = self.__chat_frame.focus
            if fc == self.__chat_frame.footer:
                self.focus_chat()
            else:
                self.focus_friend_lst()

    ### KEY MAP ###
    def keypress(self, size, key):
        if key == 'tab':
            self.change_focus()
        else:
            super(ClientView, self).keypress(size, key)



### PALETTE ###
palette_tree = [
    ('white', 'dark blue'),
    {
        'main': None,
        'chat': {
            'time': ('dark blue, bold', None),
            'name': ('light green, bold', None),
            'msg': None
        },
        'friends': {
            'status': ('light green', None),
            'name': {
                'selected': (None, 'light blue'),
                'not_selected': None,
            },
            'unread_messages': ('yellow', None)
        }
    }
]

class Palette(list):
    # Color system
    color_count = 2

    # Builder
    def __init__(self, palette_tree):
        self.__recur(None, [None] * Palette.color_count, palette_tree)

    # Update
    def __update_colors(self, colors, data):
        assert(len(data) == Palette.color_count)
        new_colors = []
        for i in xrange(len(data)):
            new_colors.append(colors[i] if data[i] == None else data[i])
        return new_colors

    # Recursive palette tree flattening
    def __recur(self, name, colors, data):
        # Recursion exit - leafs of palette tree
        if isinstance(data, tuple) or data == None:
            if data == None:
                data = [None] * Palette.color_count
            # Update colors
            new_colors = self.__update_colors(colors, data)
            # Register style
            self.append((name, new_colors[0], new_colors[1]))
        # Style class hierarchy flattening
        elif isinstance(data, dict):
            for subname, child_data in data.items():
                assert(isinstance(subname, str))
                self.__recur(
                    '%s.%s' % (name, subname) if name else subname,
                    colors,
                    child_data
                )
        elif isinstance(data, list):
            assert(isinstance(data[0], tuple) and isinstance(data[1], dict))
            new_colors = self.__update_colors(colors, data[0])
            self.__recur(name, new_colors, data[1])


if __name__ == '__main__':
    import cPickle
    with open('data.txt') as f:
        friends, history = cPickle.load(f)
    friends = friends['items']

    UI.MainLoop(UI.AttrMap(ClientView(friends), 'main'), Palette(palette_tree), handle_mouse=False).run()
