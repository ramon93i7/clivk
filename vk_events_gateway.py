from vkapi import BasePoller
from managable_thread import ManagableThread
from event_handling import EventQueue
from constants import EventCodes

class VKEventCodes:
    mapping_to_internal_codes = {
        # Flags
        1: EventCodes.CHANGE_MSG_FLAG,
        2: EventCodes.SET_MSG_FLAG,
        3: EventCodes.RESET_MSG_FLAG,
        # Send message
        4: EventCodes.ADD_NEW_MSG,
        # Update state of messages
        6: EventCodes.READ_INPUT_MSG,
        7: EventCodes.READ_OUTPUT_MSG,
        # Friends
        8: EventCodes.FRD_ON,
        9: EventCodes.FRD_OFF,
        61: EventCodes.FRD_TYPING
    }

class VKFlags:
    flags = {
        'unread': 1,
        'outbox': 2,
        'friends': 32,
        'deleted': 128
        #'media': 512
    }
    @staticmethod
    def parse_flags(flag_code):
        return { k: bool(flag_code & v) for k, v in VKFlags.flags.items() }

class VKEventsGateway(ManagableThread, BasePoller):
    # Init
    def __init__(*args, **kwargs):
        self = args[0]
        BasePoller.__init__(*args, **kwargs)
        ManagableThread.__init__(self)
        # Main flow
        self.run = self.serve
        # Determine stop-polling condition
        self.stop_condition = self.stopped
        # Event code handlers
        self.__handler_table = {
            # Message flags events
            EventCodes.CHANGE_MSG_FLAG: self.__change_msg_flag_event_handler,
            EventCodes.SET_MSG_FLAG: self.__set_msg_flag_event_handler,
            EventCodes.RESET_MSG_FLAG: self.__reset_msg_flag_event_handler,
            # New message event
            EventCodes.ADD_NEW_MSG: self.__new_msg_event_handler,
            # Read messages events
            EventCodes.READ_INPUT_MSG: self.__read_input_msgs_event_handler,
            EventCodes.READ_OUTPUT_MSG: self.__read_output_msgs_event_handler,
            # Friend status events
            EventCodes.FRD_ON: self.__friend_online_event_handler,
            EventCodes.FRD_OFF: self.__friend_offline_event_handler,
            EventCodes.FRD_TYPING: self.__friend_typing_event_handler
        }

    # Message flags events management
    def __change_msg_flag_event_handler(self, params):
        flags = VKFlags.parse_flags(params[1])
        self.__change_msg_flags(params[0], flags)

    def __set_msg_flag_event_handler(self, params):
        flags = {k: v for k, v in VKFlags.parse_flags(params[1]).items() if v}
        self.__change_msg_flags(params[0], flags)

    def __reset_msg_flag_event_handler(self, params):
        flags = {k: v for k, v in VKFlags.parse_flags(params[1]).items() if not v}
        self.__change_msg_flags(params[0], flags)

    def __change_msg_flags(self, message_id, flags):
        self.__register_event((
            EventCodes.CHANGE_MSG_FLAG, {
                'message_id': message_id,
                'flags': flags
            }))

    # New message event management
    def __new_msg_event_handler(self, params):
        self.__register_event((
            EventCodes.ADD_NEW_MSG,
            {
                'message_id': params[0],
                'flags': VKFlags.parse_flags(params[1]),
                'friend_id': abs(params[2]),
                'timestamp': params[3],
                'text': params[5]
            }
        ))

    # Read messages events management
    def __read_input_msgs_event_handler(self, params):
        self.__read_messages('input', params['peer_id'], params['local_id'])

    def __read_output_msgs_event_handler(self, params):
        self.__read_messages('output', params['peer_id'], params['local_id'])

    def __read_messages(self, filter_type, from_id, to_id):
        self.__register_event((
            EventCodes.READ_MSGS, {
                'filter': filter_type,
                'from_id': from_id,
                'to_id': to_id
            }))

    # Friend status events
    def __friend_online_event_handler(self, params):
        self.__friend_change_status(abs(params[0]), True)

    def __friend_offline_event_handler(self, params):
        self.__friend_change_status(abs(params[0]), False)

    def __friend_change_status(self, friend_id, status):
        self.__register_event((
            EventCodes.FRD_STATUS,
            {
                'friend_id': friend_id,
                'status': status
            }
        ))

    def __friend_typing_event_handler(self, params):
        self.__register_event((
            EventCodes.FRD_TYPING,
            {
                'friend_id': abs(params[0]),
            }
        ))

    def link_to_event_gateway(self, event_gateway):
        self.__register_event = event_gateway.register_event

    def handle(self, update):
        assert(self.__register_event)
        event_code = VKEventCodes.mapping_to_internal_codes[update[0]]
        params = update[1:]
        if event_code in self.__handler_table:
            self.__handler_table[event_code](params)
